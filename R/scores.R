.score <- function(stats, current_score, positive_func, negative_func, k = 5) {
    tp_count <- stats$true_positives_count - current_score$true_positives
    fp_count <- stats$false_positives_count - current_score$false_positives

    pos_res <- positive_func(tp_count)
    neg_res <- negative_func(fp_count)
    score <- pos_res / (pos_res + neg_res + k)

    list(score = score, true_positives = tp_count, false_positives = fp_count)
}


#' Returns the tozero score of a given DNF.
#' @param dnf A logical matrix representing a DNF.
#' @param population The set of considered elements.
#' @param expected_closures The elementary closures of the elements in \code{population}.
#' @param neighborhoods A list of logical matrix representing neighborhoods.
#' @param equivalence_classes A logical matrix whose rows represent an equivalence class.
#' @param current_score A list modeling the score obtained by the DNF learned at a previous step.
#'                      It contains the score ($score), the true positives count ($true_positives)
#'                      and the false positives count ($false_positives).
#' @param k A number.
#' @param use_gmp Does the big integer library must by used to count the number of true positives?
#' @return A list representing the score ($score) of the DNF, the number of new true positive covered bags ($true_positives)
#'         and the number of new false positive covered bags ($false_positives).
#' @seealso Other score methods: \code{\link{minus_negative_score}}, \code{\link{positives_count_score}}, 
#'                               \code{\link{squared_negatives_score}}, \code{\link{log_positives_score}}
tozero_score <- function(dnf, population, expected_closures, neighborhoods, equivalence_classes, current_score, k = 5, use_gmp = FALSE) {
    new_stats <- covered_bags(dnf, population, expected_closures, neighborhoods, equivalence_classes, use_gmp = use_gmp)
    .score(new_stats, current_score, positive_func = identity, negative_func = identity)
}


#' Returns the minus negative score of a given DNF.
#' @param dnf A logical matrix representing a DNF.
#' @param population The set of considered elements.
#' @param expected_closures The elementary closures of the elements in \code{population}.
#' @param neighborhoods A list of logical matrix representing neighborhoods.
#' @param equivalence_classes A logical matrix whose rows represent an equivalence class.
#' @param current_score A list modeling the score obtained by the DNF learned at a previous step.
#'                      It contains the score ($score), the true positives count ($true_positives)
#'                      and the false positives count ($false_positives).
#' @param k A number.
#' @param use_gmp Does the big integer library must by used to count the number of true positives?
#' @return A list representing the score ($score) of the DNF, the number of new true positive covered bags ($true_positives)
#'         and the number of new false positive covered bags ($false_positives).
#' @seealso Other score methods: \code{\link{tozero_score}}, \code{\link{positives_count_score}}, 
#'                               \code{\link{squared_negatives_score}}, \code{\link{log_positives_score}}
minus_negative_score <- function(dnf, population, expected_closures, neighborhoods, equivalence_classes, current_score, k = 5, use_gmp = FALSE) {
    new_stats <- covered_bags(dnf, population, expected_closures, neighborhoods, equivalence_classes, use_gmp = use_gmp)

    tp_count <- new_stats$true_positives_count - current_score$true_positives
    fp_count <- new_stats$false_positives_count - current_score$false_positives

    list(score = -fp_count, true_positives = tp_count, tn = fp_count)
}


#' Returns the positive count score of a given DNF.
#' @param dnf A logical matrix representing a DNF.
#' @param population The set of considered elements.
#' @param expected_closures The elementary closures of the elements in \code{population}.
#' @param neighborhoods A list of logical matrix representing neighborhoods.
#' @param equivalence_classes A logical matrix whose rows represent an equivalence class.
#' @param current_score A list modeling the score obtained by the DNF learned at a previous step.
#'                      It contains the score ($score), the true positives count ($true_positives)
#'                      and the false positives count ($false_positives).
#' @param k A number.
#' @param use_gmp Does the big integer library must by used to count the number of true positives?
#' @return A list representing the score ($score) of the DNF, the number of new true positive covered bags ($true_positives)
#'         and the number of new false positive covered bags ($false_positives).
#' @seealso Other score methods: \code{\link{tozero_score}}, \code{\link{minus_negative_score}}, 
#'                               \code{\link{squared_negatives_score}}, \code{\link{log_positives_score}}
positives_count_score <- function(dnf, population, expected_closures, neighborhoods, equivalence_classes, current_score, k = 5, use_gmp = FALSE) {
    new_stats <- covered_bags(dnf, population, expected_closures, neighborhoods, equivalence_classes, use_gmp = use_gmp)

    tp_count <- new_stats$true_positives_count - current_score$true_positives
    fp_count <- new_stats$false_positives_count - current_score$false_positives

    list(score = tp_count, true_positives = tp_count, tn = fp_count)
}


#' Returns the squared negative score score of a given DNF.
#' @param dnf A logical matrix representing a DNF.
#' @param population The set of considered elements.
#' @param expected_closures The elementary closures of the elements in \code{population}.
#' @param neighborhoods A list of logical matrix representing neighborhoods.
#' @param equivalence_classes A logical matrix whose rows represent an equivalence class.
#' @param current_score A list modeling the score obtained by the DNF learned at a previous step.
#'                      It contains the score ($score), the true positives count ($true_positives)
#'                      and the false positives count ($false_positives).
#' @param k A number.
#' @param use_gmp Does the big integer library must by used to count the number of true positives?
#' @return A list representing the score ($score) of the DNF, the number of new true positive covered bags ($true_positives)
#'         and the number of new false positive covered bags ($false_positives).
#' @seealso Other score methods: \code{\link{tozero_score}}, \code{\link{minus_negative_score}}, 
#'                               \code{\link{positives_count_score}}, \code{\link{log_positives_score}}
squared_negatives_score <- function(dnf, population, expected_closures, neighborhoods, equivalence_classes, current_score, k = 5, use_gmp = FALSE) {
    new_stats <- covered_bags(dnf, population, expected_closures, neighborhoods, equivalence_classes, use_gmp = use_gmp)
    .score(new_stats, current_score, positive_func = identity, negative_func = function(n) n * n)
}


#' Returns the log positives score of a given DNF.
#' @param dnf A logical matrix representing a DNF.
#' @param population The set of considered elements.
#' @param expected_closures The elementary closures of the elements in \code{population}.
#' @param neighborhoods A list of logical matrix representing neighborhoods.
#' @param equivalence_classes A logical matrix whose rows represent an equivalence class.
#' @param current_score A list modeling the score obtained by the DNF learned at a previous step.
#'                      It contains the score ($score), the true positives count ($true_positives)
#'                      and the false positives count ($false_positives).
#' @param k A number.
#' @param use_gmp Does the big integer library must by used to count the number of true positives?
#' @return A list representing the score ($score) of the DNF, the number of new true positive covered bags ($true_positives)
#'         and the number of new false positive covered bags ($false_positives).
#' @seealso Other score methods: \code{\link{tozero_score}}, \code{\link{minus_negative_score}}, 
#'                               \code{\link{positives_count_score}}, \code{\link{squared_negatives_score}}, 
log_positives_score <- function(dnf, population, expected_closures, neighborhoods, equivalence_classes, current_score, k = 5, use_gmp = FALSE) {
    bags_stats <- covered_bags(dnf, population, expected_closures, neighborhoods, equivalence_classes, use_gmp = use_gmp)
    .score(bags_stats, current_score, positive_func = log2, negative_func = identity)
}