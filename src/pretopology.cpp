#include <algorithm>
#include <iostream>
#include <iterator>
#include <Rcpp.h>

// [[Rcpp::plugins(cpp14)]]

class NeighborhoodPredicate
{
public:
    NeighborhoodPredicate(const Rcpp::LogicalMatrix& neighborhood): 
        neighborhood(neighborhood),
        elem_count(neighborhood.ncol())
    {

    }

    bool operator()(const Rcpp::LogicalVector& A, int x) const
    {
        return Rcpp::sum(neighborhood.row(x) & A) > 0;
    }

private:
    Rcpp::LogicalMatrix neighborhood;
    unsigned int elem_count;
};


class ConjunctiveClause
{
public:
    ConjunctiveClause(const std::vector<NeighborhoodPredicate>& predicates):
        literals(predicates)
    {

    }

    bool operator()(const Rcpp::LogicalVector& A, int x) const
    {
        return std::all_of(std::begin(literals), std::end(literals), [&A, x](auto& q) { return q(A, x); });
    }

private:
    std::vector<NeighborhoodPredicate> literals;
};


class DNF
{
public:
    DNF(const std::vector<ConjunctiveClause>& clauses):
        clauses(clauses)
    {

    }

    DNF(const Rcpp::LogicalMatrix& dnf_matrix, const Rcpp::List& neighborhoods)
    {
        clauses.reserve(dnf_matrix.nrow());
        for (auto i(0) ; i < dnf_matrix.nrow() ; ++i)
        {
            std::vector<NeighborhoodPredicate> preds;
            for (auto j(0) ; j < dnf_matrix.ncol() ; ++j)
            {
                if (dnf_matrix(i, j))
                {
                    Rcpp::LogicalMatrix m = neighborhoods[j];
                    preds.emplace_back(m);
                }
            }
            clauses.emplace_back(preds);
        }
    }

    bool operator()(const Rcpp::LogicalVector& A, int x) const
    {
        return std::any_of(std::begin(clauses), std::end(clauses), [&A, x](auto& c) { return c(A, x); });
    }

private:
    std::vector<ConjunctiveClause> clauses;
};


Rcpp::LogicalVector pseudoclosure(const Rcpp::LogicalVector& A, int elem_count, const DNF& model) 
{
    Rcpp::LogicalVector result(elem_count);

    for (int x(0) ; x < elem_count ; ++x)
    {
        result[x] = model(A, x);
    }

    return result;
}


//' Compute the pseudoclosure of a set.
//' @param A The set to expand.
//' @param elem_count Number of element in the pretopological space.
//' @param neighborhoods List of neighborhoods.
//' @param dnf A DNF as a logical matrix.
// [[Rcpp::export(".pseudoclosure")]]
Rcpp::LogicalVector pseudoclosure(const Rcpp::LogicalVector& A, int elem_count, const Rcpp::List& neighborhoods, const Rcpp::LogicalMatrix& dnf) 
{
    DNF model(dnf, neighborhoods);
    return pseudoclosure(A, elem_count, model);
}


//' Compute the closure of a set.
//' @param A The set to expand.
//' @param elem_count Number of element in the pretopological space.
//' @param neighborhoods List of neighborhoods.
//' @param dnf A DNF as a logical matrix.
// [[Rcpp::export(".closure")]]
Rcpp::LogicalVector closure(const Rcpp::LogicalVector& A, int elem_count, const Rcpp::List& neighborhoods, const Rcpp::LogicalMatrix& dnf) 
{
    DNF model(dnf, neighborhoods);
    Rcpp::LogicalVector result(pseudoclosure(A, elem_count, model));
    bool has_changed(true);

    while(has_changed)
    {
        auto new_result = pseudoclosure(result, elem_count, model);
        has_changed = Rcpp::is_false(Rcpp::all(result == new_result));
        result = new_result;
    }

    return result;
}