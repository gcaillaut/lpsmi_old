#include <Rcpp.h>

// [[Rcpp::plugins(cpp14)]]

//' Compute the transitive closure of a graph.
//' @param mat An adjacency matrix.
//' @return An adjacency matrix.
// [[Rcpp::export]]
Rcpp::LogicalMatrix transitive_closure(Rcpp::LogicalMatrix& mat)
{
    const unsigned int n = mat.ncol();

    for (auto k = 0u ; k < n ; ++k)
    {
        auto row = mat.row(k);
        auto col = mat.column(k);

        for (auto i = 0u ; i < n ; ++i)
        {
            for (auto j = 0u ; j < n ; ++j)
            {
                if (col[i] && row[j])
                {
                    mat(i, j) = true;
                }
            }   
        }
    }

    return mat;
}