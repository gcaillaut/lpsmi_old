# LPSMI

Une approche supervisée et multi-instances pour l'apprentissage d'espaces prétopologiques.


## Installation

### Installer R

La première étape consiste à installer R. 

Si vous utilisez Windows, téléchargez l'installateur depuis le [site officiel](https://www.r-project.org/).
Vous aurez aussi besoin d'installer les outils de développement R (Rtools) afin de compiler lpsmi.
Prenez soin de vérifier que les outils de développement R sont dans votre PATH.

Si vous utilisez une distribution Linux basée sur Debian, installer le paquet `r-base-dev` devrait être suffisant.

### Installer le paquet lpsmi

Lancez un terminal R, en tapant `R` depuis votre console. Si vous êtes sous Windows, le dossier contenant le
fichier *R.exe* doit être présent dans votre PATH.

Installez le paquet **devtools** en entrant la commande `install.packages("devtools")`. **devtools** apporte un 
ensemble d'outils simplifiant le développement et l'installation de paquets R.

Une fois **devtools** installé, vous pourrez installer le paquet **lpsmi** depuis l'adresse du dépôt avec
la commande :

```R
devtools::install_bitbucket("gcaillaut/lpsmi")
library(lpsmi) # Pour importer le paquet lpsmi
```


## Apprendre un espace prétopologique


Le moyen le plus simple pour apprendre un espace prétopologique cible est d'appeler la fonction `lpsmi`
avec deux paramètres : 
 1. l'ensemble des fermés élémentaires cibles sous la forme d'une matrice logique où une ligne représente un fermé élémentaire.
 2. Une liste de voisinages sous la forme de matrices logiques où une ligne représente les voisins d'un élément.


### Construction du jeu de données d'entraînement

Nous considérerons l'ensemble d'éléments {a, b, c, d} tel que :

- F({a}) = {a, b, c, d}
- F({b}) = {b, c, d}
- F({c}) = {c}
- F({d}) = {d}

![Fermés élémentaires attendus](resources/expected.svg)

Nous traduisons ces information en R sous la forme d'une matrice logique 4x4 où chaque ligne représente un fermé élémentaire
et chaque colonne indique l'appartenance (ou non) d'un élément à un fermé élémentaire.

```r
n <- 4
population <- letters[1:4]

expected <- matrix(c(
    1, 1, 1, 1,
    0, 1, 1, 1,
    0, 0, 1, 0,
    0, 0, 0, 1),
    nrow = n, ncol = n, byrow = TRUE,
    dimnames = list(population, population)
)
```

Si on dispose d'une matrice d'adjacence, on peut calculer une matrice de fermés élémentaires avec la fonction `transitive_closure`.
Il faudra cependant s'assurer que la matrice d'adjacence est réflexive.

```r
diag(adj) <- TRUE
expected <- transitive_closure(adj)
```

Construisons deux voisinages V1 et V2 tels que :

| x | V1(x)  | V2(x)  |
|---|--------|--------|
| a | {a}    | {a}    |
| b | {b, a} | {b, a} |
| c | {c, b} | {c}    |
| d | {d}    | {d, b} |

![Voisinage V1](resources/v1.svg)
![Voisinage V2](resources/v2.svg)

Nous modéliserons un voisinage comme une matrice logique 4x4 où une ligne représente le voisinage d'un élément et une
colonne l'appartenance d'un élément à un voisinage donné.

```r
V1 <- matrix(c(
    1, 0, 0, 0,
    1, 1, 0, 0,
    0, 1, 1, 0,
    0, 0, 0, 1),
    nrow = n, ncol = n, byrow = TRUE,
    dimnames = list(population, population)
)

V2 <- matrix(c(
    1, 0, 0, 0,
    1, 1, 0, 0,
    0, 0, 1, 0,
    0, 1, 0, 1),
    nrow = n, ncol = n, byrow = TRUE,
    dimnames = list(population, population)
)

neighborhoods <- list(V1, V2)
```

Il est important de noter que les noms attribués aux lignes et aux colonnes des matrices considérées (l'attribut `dimnames`)
doivent être identiques et ordonnés de la même façon. Ainsi, si les lignes de la matrice représentant les fermés élémentaires sont respectivement nommées "a", "b", "c" et "d", alors ses colonnes devront être nommées "a", "b", "c" et "d".
Les voisinages considérés devront aussi respecter cette propriété.

Nous disposons alors d'un jeu de données simple permettant d'apprendre une fonction d'adhérence définie par une DNF positive.
Il est évident que la DNF à apprendre est *(V1) \\/ (V2)*, appliquons la méthode lpsmi et vérifions qu'elle retourne le résultat attendu :

```r
library(lpsmi)
dnf <- lpsmi(expected, neighborhoods)
dnf
     [,1] [,2]
[1,]    1    0
[2,]    0    1
```

La fonction `lpsmi` retourne une matrice logique où :

- Une ligne correspond à une clause conjonctive
- Une colonne correspond à un littéral/voisinage

La matrice retournée est une matrice 2x2, ce qui signifie que la DNF apprise est constituée de deux clauses conjonctives.
La première est représentée par le vecteur (1, 0), soit la clause *V1*.
La seconde est représentée par le vecteur (0, 1), soit la clause *V2*.
La DNF apprise est donc bien celle attendue : *(V1) \\/ (V2)*.


### Utilisation des jeux de données inclus

Le paquet **lpsmi** inclut 4 jeux de données :

- Wagons
- Crafts
- Vehicles
- Plants

Chacun de ces jeux de données est représenté par une liste possédant 4 éléments :

- `population` : l'ensemble des éléments considérés
- `closures` : la matrice des fermés élémentaires attendus
- `hits` : la matrice des hits extraite de Wikipédia
- `patterns` : Une matrice issue de patrons syntaxiques extraits de Wikipédia

On va construire une liste de voisinages à partir de la matrice de hits. Pour cela, on utilise la 
fonction `build_neighborhoods`. Pour construire un voisinage à partir de la matrice des patrons, il suffit de calculer
la transposée de la matrice.

```r
library(lpsmi)

data(wagons) # Chargement du jeu de données 'Wagons'
neighborhoods_from_hits <- build_neighborhoods(wagons$hits)
neighborhoods_from_patterns <- list(t(wagons$patterns))
neighborhoods <- c(neighborhoods_from_hits, neighborhoods_from_patterns)
```

On peut alors apprendre une DNF en combinant ces voisinages :

```r
dnf <- lpsmi(wagons$closures, neighborhoods) # Apprentissage d'une DNF

# Construction des fermés élémentaires appris
predicted_closures <- all_elementary_closures(wagons$population, neighborhoods, dnf, output_mode = "logical")

# Calcul de la Fmesure
fmeasure(predicted_closures, wagons$closures)

# Construction d'un dag à partir des fermés élémentaires
dag <- build_dag(predicted_closures)

# Export du dag dans le langage dot
write(to_dot(dag), "wagons.dot")
```

## Les fonctions de prétopologie

Les opérateurs d'adhérence et de fermeture sont implémentés par, respectivement, les fonctions `pseudoclosure` et `closure`.
Les paramètres attendus par ces deux fonctions sont identiques :

1. `A` l'ensemble à étendre, sous la forme d'un vecteur logique ou d'un vecteur de caractères
2. `E` l'ensemble des éléments considérés
3. `neighborhoods` la liste des voisinages sur les éléments de `E`
4. `dnf` une DNF
5. (facultatif) `output_mode` définit le type retourné par la fonction. Vaut soit "logical", soit "character", s'il n'est pas renseigné, la fonction retournera le même type que `A`

```r
a <- pseudoclosure("wagons", wagons$population, neighborhoods, dnf, output_mode = "logical")
a # Un vecteur logique représentant a({wagons})
```

On peut aussi calculer l'ensemble des fermés élémentaires d'un espace prétopologique avec la fonction `all_elementary_closures`
qui attend 4 paramètres:

1. `population` l'ensemble des éléments considérés
2. `neighborhoods` la liste des voisinages considérés
3. `dnf` la DNF, sous la forme d'une matrice logique, avec autant de colonnes qu'il y a de voisinages
4. (facultatif) `output_mode` par défaut, la fonction retournera une liste de vecteur de chaîne de caractère.
Si `output_mode` vaut "logical", alors la fonction retournera une matrice logique.


## Appliquer un modèle à un autre ensemble

On a vu comment apprendre et visualiser un espace prétopologique dont les fermés élémentaires (sa structure sous jacente) sont en accords avec un ensemble de fermés cibles. Étant donné qu'on connaissait déjà les fermés élémentaires, il n'est pas très intéressant d'apprendre un modèle pour les recalculer. Ce modèle peut cependant permettre de reconstruire une structure sur un plus grand ensemble d'éléments.

Le code suivant montre (1) comment apprendre un espace prétopologique sur les éléments de la taxonomie de "crafts" et (2) comment reconstruire la taxonomie de "vehicles" à partir de ce modèle.

```r
data(crafts)
data(vehicles)

neighborhoods_crafts <- c(build_neighborhoods(crafts$hits), list(t(crafts$patterns)))
neighborhoods_vehicles <- c(build_neighborhoods(vehicles$hits), list(t(vehicles$patterns)))

dnf <- lpsmi(crafts$closures, neighborhoods_crafts)

predicted_vehicles <- all_elementary_closures(vehicles$population, neighborhoods_vehicles, dnf, output_mode = "logical")
fmeasure(predicted_vehicles, vehicles$closures)
```